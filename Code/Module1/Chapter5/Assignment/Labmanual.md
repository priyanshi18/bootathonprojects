<u> **<span style="color:blue"> Lab Manual </span>** </u>

**INFIX TO POSTFIX CONVERSION**


**AIM**

To understand the operations of stack.

**THEORY**

One of the applications of Stack is in the conversion of arithmetic expressions in high-level programming languages into machine readable form. <br>As our computer system can only understand and work on a binary language, it assumes that an arithmetic operation can take place in two operands only e.g.,A+B, C*D,D/A etc.<br> *But in our usual form an arithmetic expression may consist of more than one operator and two operands* e.g. (A+B)*C(D/(J+D)). These complex arithmetic operations can be converted into polish notation using stacks which then can be executed in two operands and an operator form.<br>
***Infix Expression***<br>
It follows the scheme of ***operand operator operand*** i.e. an **operator** preceded and succeeded by an **operand**. Such an expression is termed infix. expression. E.g., **A+B**<br>
***Postfix Expression*** <br>
It follows the scheme of i.e. an **operator** is succeeded by both the **operand **. E.g., **AB+**

<br>

**Algorithm**<br>
Let, X is an arithmetic expression written in infix notation. <br>
This algorithm finds the equivalent postfix expression Y.<br>
1.Push “(“onto Stack, and add “)” to the end of X.
<br>
2.Scan X from left to right and repeat Step 3 to 6 for each element of X until the Stack is empty.<br>
3.If an operand is encountered, add it to Y.<br>
4.If a left parenthesis is encountered, push it onto Stack.<br>
5.If an operator is encountered ,then:<br>
*Repeatedly pop from Stack and add to Y each operator (on the top of Stack)<br> *which has the same precedence as or higher precedence than operator.<br>
*Add operator to Stack.<br>
**[End of If]**<br>
6.If a right parenthesis is encountered ,then:<br>
*Repeatedly pop from Stack and add to Y each operator (on the top of Stack) <br>*until a left parenthesis is encountered.<br>
*Remove the left Parenthesis.<br>
**[End of If]<br>
[End of If]**<br>
7.END.